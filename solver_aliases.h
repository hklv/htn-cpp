#pragma once

// Solver
#include "solver_andor.h"


template<typename... Solvers>
auto solve_branch(Solvers&&... inSolvers)
{
    // Solve branch by solving first the precondition then the task list.
    return AndSolver<Solvers...>(std::forward<Solvers>(inSolvers)...);
}


template<typename... Solvers>
auto solve_task_list(Solvers&&... inSolvers)
{
    // Solve the task list by solving all of its tasks.
    return AndSolver<Solvers...>(std::forward<Solvers>(inSolvers)...);
}


template<typename... Solvers>
auto solve_prioritized(Solvers... inSolvers)
{
    // The or solves already solves its terms ordered.
    return solve_or(inSolvers...);
}
