#pragma once

//
// Applies a lambda function *inOperation* with the arguments stored in *inTuple*.
// A quick alternative for std::apply (available from c++17).
//
// Funny video here:
// The Way of the Exploding Tuple by Andrei Alexandrescu.
// https://channel9.msdn.com/Events/GoingNative/2013/The-Way-of-the-Exploding-Tuple
//

template<int n>
struct ApplyArgs
{
    template<typename Func, typename Tuple, typename... Args>
    static auto Apply(Func&& inFunc, Tuple&& inTuple, Args&&... inRestArgs)
    {
        // Apply first `n` arguments from tuple `inTuple` together with `inRestArgs` to function `inOperation`.
        // This cannot be done directly. Extract element `n-1` from the tuple and apply the rest of the tuple
        // arguments in `ApplyArgs<n-1>::Apply`.

        return ApplyArgs<n-1>::Apply(std::forward<Func>(inFunc), std::forward<Tuple>(inTuple), std::get<n-1>(inTuple), std::forward<Args>(inRestArgs)...);
    }
};

template<>
struct ApplyArgs<0>
{
    template<typename Func, typename Tuple, typename... Args>
    static auto Apply(Func&& inFunc, Tuple&& /** inTuple (unused) **/, Args&&... inRestArgs)
    {
        // Apply the function `inOperation`. All arguments necessary for the function
        // are forwarded in `inTupleArgs`.

        return std::forward<Func>(inFunc)(std::forward<Args>(inRestArgs)...);
    }
};

template<typename Func, typename Tuple>
auto apply(Func&& inFunc, Tuple& inTuple)
{
    constexpr int n = std::tuple_size<Tuple>::value;

    return ApplyArgs<n>::Apply(std::forward<Func>(inFunc), std::forward<Tuple>(inTuple));
}


