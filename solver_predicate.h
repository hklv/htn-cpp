#pragma once

#include "solver_expr.h"


template<typename... OtherTerms>
struct TermUnifier
{
    template<typename Fact>
    bool Unify(Fact&&, int)
    {
        return true;
    }
};


template<typename Term, typename... OtherTerms>
struct TermUnifier<Term, OtherTerms...> : public TermUnifier<OtherTerms...>
{
    TermUnifier<Term, OtherTerms...>(Term&& inTerm, OtherTerms&&... inTs) :
        TermUnifier<OtherTerms...>(std::forward<OtherTerms>(inTs)...),
        mTerm(std::forward<Term>(inTerm))
    {
    }

    template<typename AnyExpr>
    bool Unify(atom inPredicateValue, AnyExpr& inMatchValue)
    {
        std::cout << " " << inPredicateValue << " == " << inMatchValue.Eval() << ")?";
        return inPredicateValue == inMatchValue.Eval();
    }

    bool Unify(atom inPredicateValue, BindExpr& outBindValue)
    {
        std::cout << " (" << outBindValue.mValue << " = " << inPredicateValue << ", true)";
        *outBindValue.mValue = inPredicateValue;
        return true;
    }


    template<typename Fact>
    bool Unify(Fact&& inFact, int inIndex = 0)
    {
        return Unify(inFact[inIndex], mTerm) & TermUnifier<OtherTerms...>::Unify(inFact, inIndex+1);
    }

    Term mTerm;
};


template<typename FactList, typename... Terms>
struct PredicateSolver
{
    template<typename NextSolver>
    bool Solve(NextSolver&& inNextSolver)
    {
        for (int i=0, n=mFactList.size(); i<n; ++i)
        {
            const auto& fact = mFactList[i];

            std::cout << "predicate " << &fact << " - ";
            if (mTermUnifier.Unify(fact))
            {
              std::cout << std::endl;
              return inNextSolver();
            }
            std::cout << std::endl;
        }

        return false;
    }

    const FactList& mFactList;
    TermUnifier<Terms...> mTermUnifier;
};


template<typename FactList, typename... Terms>
auto solve_predicate(FactList&& inFactList, Terms&&... inTerms)
{
    return PredicateSolver<FactList, decltype(create_expr(std::forward<Terms>(inTerms)))...>{ std::forward<FactList>(inFactList), { create_expr(std::forward<Terms>(inTerms))... }};
}

