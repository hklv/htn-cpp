#include <iostream>

// Solver.
#include "solver.h"

facts<1> is_sleeping;
facts<1> is_running;
facts<1> happy;
facts<2> aap;
facts<2> noot;
facts<2> appel;
facts<2> mies;
facts<3> papa;
facts<2> huidige_plek;
facts<2> plek;
facts<3> visible;


axiom_t zichtbare_plek(atom *p)
{
    atom c;

    return solve_and(
                solve_predicate(huidige_plek, &c),
                solve_predicate(plek, p),
                solve_predicate(visible, c, *p));
}


task_t test_axiom()
{
    atom p;

    return solve_prioritized(
        solve_branch(
            solve_task(zichtbare_plek, &p),
            solve_task_list(
                return_lambda([&] () {
                    std::cout << "with p: " << p << "!";
                })
            )
        ));
}


task_t sleep(atom) {

    return solve_and(
        solve_add(is_sleeping),
        return_lambda([=] () { std::cout << " [!sleep]"; })
    );
}


task_t run() {

    return solve_and(
        return_lambda([=] () { std::cout << " [!run " << ((is_sleeping.size() == 1) ? "sleeping" : "awake") << "]"; }),
        solve_delete(is_sleeping),
        return_lambda([=] () { std::cout << " [!run " << ((is_sleeping.size() == 1) ? "sleeping" : "awake") << "]"; })
    );
}


task_t test_effects()
{
    atom p;

    return solve_prioritized(
        solve_branch(
            solve_task_list(
                solve_task(sleep, p),
                solve_task(run)
            )
        ));
}


func_t mul(atom a, atom b) {
    return atom(*a.As<int>() * *b.As<int>());
}


task_t poep(atom a) {

    return solve_and(
        solve_delete(noot, a),
        solve_add(noot, func(mul, 3, a)),
        return_lambda([=] () { std::cout << " [!poep " << a << "]"; })
    );
}


task_t nootjes(atom a) {

    atom b,c,y;

    return solve_prioritized(
        solve_branch(
            solve_and(
                solve_predicate(aap, 1),
                solve_predicate(papa, 3, 4)
            ),
            solve_task_list(
                solve_task(poep, func(mul, a, a))
            )
        ));
}


task_t behave(atom a) {

    atom b,c,y;

    return solve_prioritized(
        solve_branch(
            solve_or(
                solve_and(
                    solve_predicate(noot, &b),
                    solve_predicate(appel, a)
                ),
                solve_and(
                    solve_predicate(noot, &c),
                    solve_predicate(mies, func(mul, c, 1))
                )
            ),
            solve_task_list(
                solve_task(nootjes, func(mul, func(mul, 4, func(mul, 4, 2)), 3)),
                solve_task(nootjes, 3)
            )
        ));
}




int main()
{
    std::cout << "Planner test" << std::endl;

    std::cout << std::endl << std::endl;
    std::cout << "== World State ========" << std::endl;

    add_fact(happy);

    add_fact(aap, 1);
    add_fact(aap, 2);
    add_fact(aap, 3);

    add_fact(noot, 3);
    add_fact(mies, 3);

    add_fact(papa, 3, 4);

    add_fact(huidige_plek, 3);
    add_fact(plek, 1);
    add_fact(plek, 2);
    add_fact(plek, 3);
    add_fact(visible, 2, 2);
    add_fact(visible, 1, 3);
    add_fact(visible, 3, 1);

    std::cout << std::endl << std::endl;
    std::cout << "== Axiom Test ==========" << std::endl;

    test_axiom();

    std::cout << std::endl << std::endl;
    std::cout << "=== Add/Del Test ===" << std::endl;

    test_effects();

    std::cout << std::endl << std::endl;
    std::cout << "=== Solver Test ===" << std::endl;

    behave(1);

    std::cout << std::endl << std::endl << std::endl;
    return 0;
}
