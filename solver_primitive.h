#pragma once

// solver
#include "solver_cc.h"


template<typename Func>
struct PrimitiveSolver
{
    template<typename NextSolver>
    bool Solve(NextSolver&& inNextSolver)
    {
      if (inNextSolver())
      {
          // The solver succeeded.
          mOnSucceedFunc();

          return true;
      }

      return false;
    }

    Func mOnSucceedFunc;
};


template<typename Func>
auto return_lambda(Func&& inFunc)
{
    return PrimitiveSolver<Func>{ std::forward<Func>(inFunc) };
}


