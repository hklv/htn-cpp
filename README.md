This is an experiment with the ~~new~~ C++11/14 language constructs. The idea is from [@SpowNzz](https://twitter.com/SpowNzz). He suggested C++ now must be expressive enough to write a Hierarchical Task Network (HTN) planning system in.

If you don't know what a HTN planner is you can read up [here](http://www.cs.umd.edu/~nau/papers/nau1999shop.pdf). Our interest in them comes from their usage in video games where they're used to author Non-Player Character (NPC) behavior. The language is based on unification / pattern matching and backtracking to explore all possible plans and determine the best one.

In most setups one would build an interpreter to execute the planner rules. This here is an attempt at a library that allows us to write these planner rules directly in C++. It aims to do this while conserving most of the original spirit of HTN, including backtracking and precondition matching.


## Similar work

There's a similar effort from Dana Nau with [pyhop](https://bitbucket.org/dananau/pyhop) (presentation [here](http://www.cs.umd.edu/~nau/papers/nau2013game.pdf)) for Python. 


## Relevant ~~new~~ features in C++

The source code extensively uses the following features:
* function objects to delay direct evaluation of matching, binding and function evaluation.
* auto keyword to simplify template functions.
* variadic templates to allow for tasks, predicates, and-/or-clauses and function calls with multiple arguments.
* perfect forwarding to reduce copying around objects to a minimum.

## Set up

World state facts are grouped into lists and identified by their first symbol for fast lookup.

```c++
facts<3> nut;          // stores (nut a b) facts for any a,b.
facts<2> apple;        // stores (apple x) facts.
```

Methods, operators and axioms are represented by functions. These functions are set up so that they construct a (composite) solver object. The solver object is then evaluated on return.

```c++
task_t behave(atom a);                // task behave
operator_t putdown(atom block);       // operator putdown
axiom_t perfect_space(atom* spot);    // axiom perfect_space
```

Variables are represented by variables but in C++ they need to be declared.

```c++
atom a, b, c;   // introduce variables a,b,c
```

Bind and matching is explicit. There's no implicit unification. In C++ we match using a reference to a variable. When we want a variable bind we provide a pointer to the variable.

```c++
atom *b, a;

solve_predicate(noot, &b),  // bind variable b
solve_predicate(appel, a)   // match variable a
```

In general (non-)deterministic functions change from lisp-style notation (func a b) to C-style func(a, b) with comma seperators.

An overview of the available functions:

HTN              | C++                              
---------------- | --------------------------------
(or a b c)       | solve_or(a, b, c)               
(and a b c)      | solve_and(a, b, c)               
(predicate a b)  | solve_predicate(predicate, a, b)
(eval x a b)     | func(x, a, b)
(branch ...)     | solve_branch(...)
((task ...) ...) | solve_task_list( solve_task(...), ... )

<br/>

## Preview

Here's a preview of how it looks like when you write a task:
```c++

task_t behave(atom a) {

    atom b,c,y;

    return solve_prioritized(
        solve_branch(
            solve_or(
                solve_and(
                    solve_predicate(noot, &b),
                    solve_predicate(appel, a)
                ),
                solve_and(
                    solve_predicate(noot, &c),
                    solve_predicate(mies, func(mul, c, 1))
                )
            ),
            solve_task_list(
                solve_task(iets, func(mul, func(mul, 4, func(mul, 4, 2)), 3)),
                solve_task(iets_anders, 3)
            )
        ));

}

```

If you'd want, you could probably make the syntax more terse. You could probably hide the  `solve_predicate` function and instead of `solve_or`, `solve_and`
you could overload bitwise or/and operator. To me, that would be less readable as that would create very dense preconditions. I like the information density to be more or less consistent over all statements.


## Source Code

The source code consists of a few small header files and an example cpp file.
You can compile it simply by:

```
  > clang++ -std=c++14 main.cpp
  > ./a.out
```

## Final Remarks

Please be aware that this code hasn't been thoroughly tested and is written for experimental purposes only.
