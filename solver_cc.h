#pragma once


std::function<bool()> gNextSolver = [] () {

    std::cout << "-- Found Solution ---- " << std::endl;

    return true;
};


struct SolveWithNext
{
    template<typename Solver>
    SolveWithNext(Solver&& inSolver)
    {
        // There's a partial plan stored in `gNextSolver`.
        std::function<bool()> next_plan_solver = gNextSolver;

        // Solve `inSolver`. The next plan also needs to be solved in order to have a
        // solution for all the plans.
        mValue = inSolver.Solve(next_plan_solver);
    }

    operator bool()     { return mValue; }

    bool mValue;
};

using axiom_t     = SolveWithNext;
using task_t      = SolveWithNext;
using operator_t  = SolveWithNext;

using func_t  = atom;

