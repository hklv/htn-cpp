#pragma once

struct HtnTypeFunctions
{
    template<typename T>
    static const HtnTypeFunctions* sGet()
    {
        static HtnTypeFunctions sSharedInterface{ Equals<T>, Write<T> };

        return &sSharedInterface;
    }

    template<typename T>
    static bool Equals(const void* inValueA, const void* inValueB)
    {
        return *static_cast<const T*>(inValueA) == *static_cast<const T*>(inValueB);
    }

    template<typename T>
    static std::ostream& Write(std::ostream& inOutputStream, const void* inValue)
    {
        return inOutputStream << *static_cast<const T*>(inValue);
    }

    bool (*mEqualsFunc)(const void* inValueA, const void* inValueB);
    std::ostream& (*mWriteFunc)(std::ostream& inOutputStream, const void* inValue);
};



//
// Atom
//
struct atom
{
    atom() {}

    template<typename T>
    atom(const T& inValue) { mFuncs = HtnTypeFunctions::sGet<T>(); mPtr = &inValue; }

    template<typename T>
    static HtnTypeFunctions* sGetFuncs()
    {
        static HtnTypeFunctions sFuncs = HtnTypeFunctions::sGet<T>();

        return &sFuncs;
    }

    bool operator==(const atom& inOther) const
    {
        return  mFuncs == inOther.mFuncs &&
                mFuncs->mEqualsFunc(this, inOther.mPtr);
    }

    template<typename T>
    bool Is() const
    {
        if (mFuncs != HtnTypeFunctions::sGet<T>())
            return false;

        return true;
    }

    template<typename T>
    const T* As() const
    {
        if (Is<T>())
            return static_cast<const T*>(mPtr);

        return nullptr;
    }

    friend std::ostream& operator<<(std::ostream& outOutputStream, const atom& inAtom);

    const HtnTypeFunctions* mFuncs{ nullptr };
    const void* mPtr{ nullptr };
};


std::ostream& operator<<(std::ostream& outOutputStream, const atom& inAtom)
{
    return inAtom.mFuncs->mWriteFunc(outOutputStream, inAtom.mPtr);
}

template<int n>
using fact = std::array<atom, n>;

template<int n>
using facts = std::vector< fact<n-1> >;


template<typename FactList, typename... Terms>
bool add_fact(FactList& ioFactList, const Terms&... inTerms)
{
    fact<sizeof...(Terms)> entry{{ atom(inTerms)... }};

    // Search for possible duplicate.
    auto iterator = std::find(ioFactList.begin(), ioFactList.end(), entry);

    if (iterator != ioFactList.end())
    {
      // No fact needed to be added. => false.
      return false;
    }

    ioFactList.emplace_back(entry);

    std::cout << "add: ";
    print_fact(&ioFactList.back(), inTerms...);
    std::cout << std::endl;

    // Return fact list changed.
    return true;
}


template<typename FactList, typename... Terms>
bool remove_fact(FactList& ioFactList, const Terms&... inTerms)
{
    fact<sizeof...(Terms)> entry{{ atom(inTerms)... }};

    auto iterator = std::find(ioFactList.begin(), ioFactList.end(), entry);

#ifdef DEBUG_SOLVER
    if (iterator != inFactList.end())
    {
        std::cout << "remove: ";
        print_fact(&inFactList.back(), inArgs...);
        std::cout << std::endl;
    }
#endif

    // Remove matching entries.
    return ioFactList.erase(iterator);
}
