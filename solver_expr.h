#pragma once

// solver
#include "solver_facts.h"


struct VarExpr
{
    atom Eval() const
    {
        return mValue;
    }

    const atom& mValue;
};


struct ConstExpr
{
    atom Eval() const
    {
        return mValue;
    }

    atom mValue;
};


template<typename ExternalFunc, typename... Args>
struct ExternalFunction
{
    atom Eval() const
    {
        auto func = [=](const Args&... inTs) {

            // Evaluate arguments and apply function.
            return mFunc(inTs.Eval()...);
        };

        return apply(func, mArgs);
    }

    ExternalFunc mFunc;
    std::tuple<Args...> mArgs;
};


struct BindExpr
{
    atom* Eval() const
    {
        return mValue;
    }

    atom* mValue;
};


template<typename AnyValue>
auto create_expr(AnyValue inValue)
{
    return ConstExpr{ inValue };
}

template<typename... Args>
auto create_expr(ExternalFunction<Args...>&& inValue)
{
    return std::forward<ExternalFunction<Args...>>(inValue);
}

auto create_expr(const atom& inValue)
{
    return VarExpr{ inValue };
}

auto create_expr(atom* inValue)
{
    return BindExpr{ inValue };
}


template<typename T, typename... Ts>
auto func(T inFunc, Ts&&... inTs)
{
    return ExternalFunction<T, decltype(create_expr(std::forward<Ts>(inTs)))...>{ std::forward<T>(inFunc), { create_expr(std::forward<Ts>(inTs))... }};
}


