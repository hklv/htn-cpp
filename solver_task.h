#pragma once

// solver
#include "solver_cc.h"
#include "my_apply.h"

template<typename TaskFunc, typename... Args>
struct TaskSolver
{
    template<typename NextSolver>
    bool Solve(NextSolver&& inNextSolver)
    {
        std::function<bool()> old_next_task = gNextSolver;

        // For the solver to succeed we need `inNextSolver` to be solved after `mTaskFunc` is solved.
        gNextSolver = [&]() {
            return inNextSolver();
        };

        auto func = [&](const Args&... inArgs) {

            // Evaluate arguments, then call task.
            return mTaskFunc(inArgs.Eval()...);
        };

        bool is_solved = apply(func, mArgs);

        // Restore the `gNextTask`. Undo any observable effects.
        gNextSolver = old_next_task;

        return is_solved;
    }

    TaskFunc mTaskFunc;
    std::tuple<Args...> mArgs;
};


template<typename Task, typename... Args>
auto solve_task(Task inTask, Args&&... inArgs)
{
    return TaskSolver<Task, decltype(create_expr(std::forward<Args>(inArgs)))...>{ inTask, { create_expr(std::forward<Args>(inArgs))... }};
}
