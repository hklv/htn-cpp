#pragma once

void print_all()
{
}

template<typename Item, typename... OtherItems>
void print_all(Item&& inItem, OtherItems&&... inOtherItems)
{
    std::cout << " " << inItem;
    print_all(inOtherItems...);
}

template<typename Item, typename... OtherItems>
void print_fact(Item&& inItem, OtherItems&&... inOtherItems)
{
    std::cout << "(" << inItem;
    print_all(inOtherItems...);
    std::cout << ")";
}

