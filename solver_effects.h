#pragma once

// Solver
#include "my_apply.h"
#include "solver_facts.h"


template<typename FactList, typename... Args>
struct AddSolver
{
    template<typename NextSolver>
    bool Solve(NextSolver&& inNextSolver)
    {
        auto eval_func = [&](const Args&... inTs) {

            // Evaluate fact.
            return fact<sizeof...(Args)>{{ inTs.Eval()... }};
        };

        auto entry = apply(eval_func, mArgs);

        bool is_duplicate = std::find(mFactList.begin(), mFactList.end(), entry) != mFactList.end();
        if (!is_duplicate)
        {
            mFactList.emplace_back(entry);

            // Debug output.
            std::cout << "add: (" << &mFactList.back();
            for (auto& t : entry)
                std::cout << ", " << t;
            std::cout << ")" << std::endl;
        }

        // See if we can find a solution for the other constraints. Solve the rest.
        bool is_solved = inNextSolver();

        if (!is_duplicate)
        {
            // If the fact was added (no duplicate already existing).
            mFactList.pop_back();
        }

        return is_solved;
    }

    FactList& mFactList;
    std::tuple<Args...> mArgs;
};


template<typename FactList, typename... Terms>
auto solve_add(FactList&& inFactList, Terms&&... inTerms)
{
    return AddSolver<FactList, decltype(create_expr(std::forward<Terms>(inTerms)))...>{ std::forward<FactList>(inFactList), { create_expr(std::forward<Terms>(inTerms))... }};
}







template<typename FactList, typename... Terms>
struct DeleteSolver
{
    template<typename NextSolver>
    bool Solve(NextSolver&& inNextSolver)
    {
        auto eval_func = [&](const Terms&... inTerms){
            return fact<sizeof...(Terms)>{{ inTerms.Eval()... }};
        };

        auto entry = apply(eval_func, mTerms);

        auto iterator = std::find(mFactList.begin(), mFactList.end(), entry);

        bool was_removed = false;
        int index;

        if (iterator != mFactList.end())
        {
            index = iterator - mFactList.begin();

            // Debug output.
            std::cout << "del: (" << &*iterator;
            for (auto& t : entry)
                std::cout << ", " << t;
            std::cout << ")" << std::endl;

            // Delete the fact.
            mFactList.erase(iterator);

            was_removed = true;
        }

        // See if we can find a solution for the other constraints. Solve the rest.
        bool is_solved = inNextSolver();

        if (was_removed)
        {
            // Undo observable changes. Reinsert deleted entry.
            mFactList.insert(mFactList.begin() + index, entry);
        }

        return is_solved;
    }

    FactList& mFactList;
    std::tuple<Terms...> mTerms;
};


template<typename FactList, typename... Terms>
auto solve_delete(FactList&& inFactList, Terms&&... inTerms)
{
    return DeleteSolver<FactList, decltype(create_expr(std::forward<Terms>(inTerms)))...>{ std::forward<FactList>(inFactList), { create_expr(std::forward<Terms>(inTerms))... }};
}
