#pragma once

// Solver
#include "solver_basics.h"

template <typename... Solvers>
struct AndSolver : public TrueSolver
{
};


template<typename Solver, typename... OtherSolvers>
struct AndSolver<Solver, OtherSolvers...>
{
    AndSolver(Solver&& inSolver, OtherSolvers&&... inOtherSolvers) :
        mSolver(std::forward<Solver>(inSolver)), mInnerSolver(std::forward<OtherSolvers>(inOtherSolvers)...)
    {
    }

    template<typename NextSolver>
    bool Solve(NextSolver&& inNextSolver)
    {
        // Solve first term of the and.
        return mSolver.Solve([&] () {

            // Solve the other terms of the and.
            return mInnerSolver.Solve(inNextSolver);
        });
    }

    Solver mSolver;
    AndSolver<OtherSolvers...> mInnerSolver;
};



template<typename... Solvers>
auto solve_and(Solvers&&... inSolvers)
{
    return AndSolver<Solvers...>(std::forward<Solvers>(inSolvers)...);
}



template <typename... Solvers>
struct OrSolver : public FalseSolver
{
};


template<typename Solver, typename... OtherSolvers>
struct OrSolver<Solver, OtherSolvers...>
{
    OrSolver(Solver&& inSolver, OtherSolvers&&... inOtherSolvers) :
        mSolver(std::forward<Solver>(inSolver)), mInnerSolver(std::forward<OtherSolvers>(inOtherSolvers)...)
    {
    }

    template<typename NextSolver>
    bool Solve(NextSolver&& inNextSolver)
    {
        // Solve this term.
        bool is_or_solved = mSolver.Solve([&] () {
            return inNextSolver();
        });

        if (is_or_solved)
        {
            // Propagate success.
            return true;
        }

        // Try the other clauses.
        return mInnerSolver.Solve(inNextSolver);
    }

    Solver mSolver;
    OrSolver<OtherSolvers...> mInnerSolver;
};


template<typename... Solvers>
auto solve_or(Solvers&&... inSolvers)
{
    return OrSolver<Solvers...>(std::forward<Solvers>(inSolvers)...);
}

