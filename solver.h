#pragma once

// std
#include <utility>
#include <algorithm>
#include <iostream>
#include <functional>
#include <string>
#include <array>
#include <vector>
#include <tuple>
#include <type_traits>

// solver
#include "solver_debug.h"
#include "solver_basics.h"
#include "solver_andor.h"
#include "solver_expr.h"
#include "solver_predicate.h"
#include "solver_aliases.h"
#include "solver_task.h"
#include "solver_primitive.h"
#include "solver_effects.h"
