#pragma once


struct TrueSolver
{
    template<typename Solver>
    bool Solve(Solver&& inNextSolver)
    {
        return inNextSolver();
    }
};


struct FalseSolver
{
    template<typename Solver>
    bool Solve(Solver&&)
    {
        return false;
    }
};

